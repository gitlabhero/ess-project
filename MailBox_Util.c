/*
 * MailBox_Util.c
 *
 *  Created on: 17.01.2018
 *      Author: salko.nuhanovic
 */

#include "MailBox_Util.h"
#include "7Seg_Task.h"

Mailbox_Handle mailBox;
extern bool showWait;

void initMailbox(void){
    Mailbox_Params params;
    Error_Block eb;
    Error_init(&eb);

    Mailbox_Params_init(&params);

    mailBox = Mailbox_create(sizeof(MsgObj), 20, &params, &eb);
    if (mailBox == NULL) {
        System_abort("Creating the Mailbox failed.");
    }
}
void writeToMailBox(MsgObj* msg){
    Mailbox_post(mailBox, msg, BIOS_WAIT_FOREVER);
}

void pendFromMailbox(void){
    MsgObj msg;

        while (1) {
            if(Mailbox_pend(mailBox, &msg, BIOS_WAIT_FOREVER)){
                showWait = false;
                onMessageReceived(&msg);
            }
        }
}
