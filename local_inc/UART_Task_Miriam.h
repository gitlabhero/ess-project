#ifndef UART_TASK_H_
#define UART_TASK_H_

#include <stdbool.h>
#include <stdint.h>
#include <xdc/std.h>
#include <Mailbox_Util.h>


/*
 * Execute UART Task
 * lsends message given
 * param arg0 void
 * param arg1 void
 */
void UARTFxn_M(UArg arg0, UArg arg1);

/*
 * Setup UART task
 * return: 0, error: system stop.
 */
int setup_UART_Task_Miriam(void);


/*
 *  Sets value for sending
 *  param is MsgObj with value too send
 */
void setUART(MsgObj message);

#endif
