/*
 * 7Seg_Task.h
 *
 *  Created on: 15.01.2018
 *      Author: salko.nuhanovic
 */

#ifndef LOCAL_INC_7SEG_TASK_H_
#define LOCAL_INC_7SEG_TASK_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <ctype.h>
#include <inc/hw_memmap.h>

/* XDCtools Header files */
#include <xdc/std.h>
#include <xdc/cfg/global.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/Memory.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/hal/Timer.h>
#include <ti/sysbios/knl/Event.h>

/* Instrumentation headers */
#include <ti/uia/runtime/LogSnapshot.h>

/* Driverlib headers */
#include <driverlib/gpio.h>
#include <driverlib/ssi.h>

/* Board Header files */
#include <Board.h>
#include <EK_TM4C1294XL.h>

/* Application headers */


#include <driverlib/sysctl.h>
#include <driverlib/gpio.h>
#include <ti/drivers/SPI.h>
#include <driverlib/pin_map.h>
#include "7Seg_Util.h"
#include "MailBox_Util.h"

/*! \fn setup_7Seg_Task
 *  \brief Setup 7Seg task
 *
 *  Sets up 7Seg Task and Clock task which is triggering the ClockFxn.
 *
 *  \param prio priority for task
 *  \param name name of task
 *  \return 0 if sucessful
 */
int setup_7Seg_Task(int prio, xdc_String name);

/*! \fn SevenSegFxn
 *  \brief Task Function of 7Seg Task.
 *
 *  Pends for Messages via calling the pendFromMailbox function of Mailbox_Util.
 *
 *  \return void
 */
void SevenSegFxn();

/*! \fn ClockFxn
 *  \brief Clock Task Fuction of Clocl Task.
 *
 *  Used for PWM and writing the messages/wait indicators to he 7Seg Displays.
 *
 *  \return void
 */
void ClockFxn(void);

/*! \fn onMessageReceived
 *  \brief Handles Received Message.
 *
 *  Converts characters from the message object to digits / characters and prepares the next data
 *  to be written on the 7Seg Display.
 *
 *  \param msg Message struct that needs to be handled
 *  \return void
 */
void onMessageReceived(MsgObj* msg);

#endif /* LOCAL_INC_7SEG_TASK_H_ */
