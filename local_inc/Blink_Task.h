#ifndef BLINK_TASK_H_
#define BLINK_TASK_H_

#include <stdbool.h>
#include <stdint.h>
#include <xdc/std.h>


/* sets pin,port and color level
 * params: uint32_t red or blue or green in 0-255
 */
void set_Brightness(uint32_t Red_Level,uint32_t Green_Level,uint32_t Blue_Level);

/*  is called after Clock Interrupt.
 *  dims the RGB Leds
 */
void PWM_Led();

/*  setup clock-interrupt & call set brightness
 */
void Blink_Fxn(void);

/*  setup Blink task
 *  return 0, error: system stop
 */
int setup_Blink_Task(void);

#endif
