/*
 * 7Seg_Task.h
 *
 *  Created on: 15.01.2018
 *      Author: salko.nuhanovic
 */

#ifndef LOCAL_INC_7SEG_UTIL_H_
#define LOCAL_INC_7SEG_UTIL_H_

#include <xdc/std.h>
#include <xdc/cfg/global.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Error.h>

/* Sysbios Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Queue.h>
#include <ti/sysbios/knl/Event.h>
#include <ti/sysbios/hal/Hwi.h>
#include <ti/sysbios/knl/Semaphore.h>

/* 7-Click*/
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Event.h>
#include <driverlib/systick.h>

/* Drivers Header files*/
#include <ti/drivers/I2C.h>
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/i2c.h"
#include <driverlib/gpio.h>
#include <driverlib/ssi.h>
#include <driverlib/interrupt.h>
#include <ti/drivers/GPIO.h>

/*Makros Header Files */
#include "inc/hw_memmap.h"
#include "inc/hw_i2c.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "math.h"

/*Board Header files */
#include <Board.h>
#include <EK_TM4C1294XL.h>
#include "inc/tm4c1294ncpdt.h"

#include <stdint.h>
#include <string.h>
#include <stdbool.h>

/*! \fn init
 *  \brief Initializes the SPI/SSI components.
 *
 *  Initialization of the SPI/SSI components on BoosterPack2.
 *
 *  \return void
 */
void init(void);

/*! \fn writeDataTo7Seg
 *  \brief Writes data to 7Seg Display on BoosterPack 2.
 *
 *  Activates Slave Select and sends data.
 *
 *  \return void
 */
void writeDataTo7Seg(uint16_t data);

/*! \fn prepareDatafor7Seg
 *  \brief Prepares data for 2x8BitShiftregisters
 *
 *  Prepares the data for the 8-Bitshiftregisters, and adds the decimal points.
 *  \param leftDisyplay data to be shown on left display
 *  \param rightDisplay data to be shown on right display
 *  \param leftDecimal if true sets decimal point on left
 *  \param rightDecimal if true sets decimal point on right
 *  \return void
 */
uint16_t prepareDatafor7Seg(uint8_t leftDisyplay,uint8_t rightDisplay, bool leftDecimal, bool rightDecimal);

/*! \fn GetChar
 *  \brief Converts Char to value that can be passed to 7Seg Display.
 *
 *  Converts a character to the value that can be sent to 8-Bitshiftregister.
 *  \param Character value that needs to be converted
 *  \return 8bit Value for 8-Bitshiftregister.
 */
unsigned short GetChar(char Character);

/*! \fn GetDigit
 *  \brief Converts Digit to value that can be passed to 7Seg Display.
 *
 *  Converts a digit to the value that can be sent to 8-Bitshiftregister.
 *  \param digit value that needs to be converted
 *  \return 8bit Value for 8-Bitshiftregister.
 */
unsigned short GetDigit(unsigned short Digit);

/*! \fn createWaitValue
 *  \brief Provide value for wait indicator.
 *
 *  Returns the next value, out of an array, to be shown for wait indicator.
 *  \param index index for the array that holds the data
 *  \return 8bit Value for 8-Bitshiftregister.
 */
uint16_t createWaitValue(uint16_t index);

#endif /* LOCAL_INC_7SEG_UTIL_H_ */

