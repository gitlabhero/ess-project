#ifndef I2C_TASK_H_
#define I2C_TASK_H_

#include <stdbool.h>
#include <stdint.h>
#include <xdc/std.h>

/*  \copyright by example for microc (c) MikroElektronika, 2013
 *  \ Turns the RGB Floats after dividing by Clear Value to HSL
 *  return: HUE needed for color detection
 *  param: detected color divided by clear first red, then green, then blue, all float
 */
float RGB_To_HSL(float red, float green, float blue);


/*
 * Configure RGB Led PINS for Output
 */
void configure_leds();

/*
 *  starts i2c on bp1, addresses & configures color click(MANUAL!)
 *  after setup: reads from sensor (color/clear + low_byte)
 *  divides colors by clear value and does some math in order too find the color given
 *  ----> takes average of 16 measurements
 */
void I2CFxn(void);

/*
 *  Setup I2C task
 *  return: 0, error: system stop.
 */
int setup_I2C_Task(void);


#endif /* LOCAL_INC_I2C_TASK_H_ */
