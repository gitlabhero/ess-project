/*
 * Mailbox_Util.h
 *
 *  Created on: 17.01.2018
 *      Author: salko.nuhanovic
 */

#ifndef LOCAL_INC_MAILBOX_UTIL_H_
#define LOCAL_INC_MAILBOX_UTIL_H_
#include <stdbool.h>
#include <stdint.h>
#include <ctype.h>
#include <inc/hw_memmap.h>

/* XDCtools Header files */
#include <xdc/std.h>
#include <xdc/cfg/global.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/Memory.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/hal/Timer.h>
#include <ti/sysbios/knl/Event.h>

/* Instrumentation headers */
#include <ti/uia/runtime/LogSnapshot.h>

/* Driverlib headers */
#include <driverlib/gpio.h>
#include <driverlib/ssi.h>

/* Board Header files */
#include <Board.h>
#include <EK_TM4C1294XL.h>

/* Application headers */


#include <driverlib/sysctl.h>
#include <driverlib/gpio.h>
#include <ti/drivers/SPI.h>
#include <driverlib/pin_map.h>

#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/knl/Mailbox.h>
#include <ti/sysbios/knl/Event.h>
#include <ti/sysbios/hal/Timer.h>
#include <ti/sysbios/BIOS.h>


typedef struct MsgObj {
    char letter_1;
    char letter_2;
} MsgObj;

/*! \fn initMailbox
 *  \brief Creates the Mailbox.
 *
 *  Creates the Mailbox.
 *
 *  \return void
 */
void initMailbox(void);

/*! \fn writeToMailBox
 *  \brief Writes message to Mailbox.
 *
 *  Creates the Mailbox.
 *
  * \param msg Message struct which gets delivered to mailbox.
 *  \return void
 */
void writeToMailBox(MsgObj* msg);

/*! \fn pendFromMailbox
 *  \brief Gets message from Mailbox.
 *
 *  Waites for next message and triggers the process to be shown on 7Seg.
 *
 *  \return void
 */
void pendFromMailbox(void);


#endif /* LOCAL_INC_MAILBOX_UTIL_H_ */
