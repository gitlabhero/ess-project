/*
 *  ======== Blink_Task.c ========
 */



#include <stdbool.h>
#include <stdint.h>
#include <inc/hw_memmap.h>

/* XDCtools Header files */
#include <xdc/std.h>
#include <xdc/cfg/global.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/Memory.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>

/* Driverlib headers */
#include <driverlib/gpio.h>
#include <driverlib/systick.h>
#include <driverlib/sysctl.h>
#include <driverlib/interrupt.h>
#include "driverlib/pin_map.h"
#include "inc/hw_timer.h"
#include "driverlib/Timer.h"

#include <ti/sysbios/knl/Clock.h>


/* Board Header files */
#include <Board.h>
#include <EK_TM4C1294XL.h>

/* Application headers */
#include "Blink_Task.h"

#define RED 0
#define GREEN 0
#define BLUE 250



uint32_t Color_Level[3];
uint32_t Led_Port[3];
uint32_t Led_Pin[3];
uint32_t Counter;
Clock_Params clockParams;
Clock_Handle myClock;
Error_Block eb;




void set_Brightness(uint32_t Red_Level,uint32_t Green_Level,uint32_t Blue_Level){
    System_printf("------------- SET BRIGHTNESS -----------");

    Led_Pin[0] = GPIO_PIN_4;
    Led_Pin[1] = GPIO_PIN_2;
    Led_Pin[2] = GPIO_PIN_3;

	Led_Port[0] = GPIO_PORTE_BASE;
	Led_Port[1] = GPIO_PORTH_BASE;
	Led_Port[2] = GPIO_PORTM_BASE;

    Color_Level[0] = Red_Level;
    Color_Level[1] = Green_Level;
    Color_Level[2] = Blue_Level;
}


void BlinkFxn(void)
{
    System_printf("---------- BLINKY TASK -----------");
	GPIOPinTypeGPIOOutput(GPIO_PORTE_BASE, GPIO_PIN_4);
	GPIOPinTypeGPIOOutput(GPIO_PORTH_BASE, GPIO_PIN_2);
	GPIOPinTypeGPIOOutput(GPIO_PORTM_BASE, GPIO_PIN_3);

	set_Brightness(RED,GREEN, BLUE);



}

void PWM_Led(){
    int i;
        Counter=0;
        for(i = 0; i < 3; i++){
            if(Counter < Color_Level[i]){
                GPIOPinWrite(Led_Port[i], Led_Pin[i], Led_Pin[i]);
                Counter ++;
            }else{
                GPIOPinWrite(Led_Port[i], Led_Pin[i], 0);
                Counter++;
            }
        }
      Task_sleep(500);
}

/*
 *  Setup task function
 */
int setup_Blink_Task(void){
    Task_Params taskLedParams;
    Task_Handle taskLed;
    Error_Block eb;
    
    /* Create blink task with priority*/
    Error_init(&eb);
    Task_Params_init(&taskLedParams);
    taskLedParams.stackSize = 1024; /* stack in bytes */
    taskLedParams.priority = 15; /* 0-15 (15 is highest priority on default -> see RTOS Task configuration) */
    taskLed = Task_create((Task_FuncPtr)BlinkFxn, &taskLedParams, &eb);
    if (taskLed == NULL) {
        System_abort("TaskLed create failed");
    }

    Error_init(&eb);
    Clock_Params_init(&clockParams);
    clockParams.period = 1200; // periodic
    clockParams.startFlag = TRUE;
    myClock = Clock_create(PWM_Led, clockParams.period, &clockParams, &eb);
    if (myClock == NULL) {
         System_abort("Clock create failed");
    }
    return (0);
}
