################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CFG_SRCS += \
../application.cfg 

CMD_SRCS += \
../EK_TM4C1294XL.cmd 

C_SRCS += \
../7Seg_Task.c \
../7Seg_Util.c \
../Blink_Task.c \
../I2C_Task.c \
../MailBox_Util.c \
../StartBIOS.c \
../StartBIOS12.c \
../UART_Task.c \
../UART_Task_Miriam.c 

GEN_CMDS += \
./configPkg/linker.cmd 

GEN_FILES += \
./configPkg/linker.cmd \
./configPkg/compiler.opt 

GEN_MISC_DIRS += \
./configPkg/ 

C_DEPS += \
./7Seg_Task.d \
./7Seg_Util.d \
./Blink_Task.d \
./I2C_Task.d \
./MailBox_Util.d \
./StartBIOS.d \
./StartBIOS12.d \
./UART_Task.d \
./UART_Task_Miriam.d 

GEN_OPTS += \
./configPkg/compiler.opt 

OBJS += \
./7Seg_Task.obj \
./7Seg_Util.obj \
./Blink_Task.obj \
./I2C_Task.obj \
./MailBox_Util.obj \
./StartBIOS.obj \
./StartBIOS12.obj \
./UART_Task.obj \
./UART_Task_Miriam.obj 

GEN_MISC_DIRS__QUOTED += \
"configPkg/" 

OBJS__QUOTED += \
"7Seg_Task.obj" \
"7Seg_Util.obj" \
"Blink_Task.obj" \
"I2C_Task.obj" \
"MailBox_Util.obj" \
"StartBIOS.obj" \
"StartBIOS12.obj" \
"UART_Task.obj" \
"UART_Task_Miriam.obj" 

C_DEPS__QUOTED += \
"7Seg_Task.d" \
"7Seg_Util.d" \
"Blink_Task.d" \
"I2C_Task.d" \
"MailBox_Util.d" \
"StartBIOS.d" \
"StartBIOS12.d" \
"UART_Task.d" \
"UART_Task_Miriam.d" 

GEN_FILES__QUOTED += \
"configPkg/linker.cmd" \
"configPkg/compiler.opt" 

C_SRCS__QUOTED += \
"../7Seg_Task.c" \
"../7Seg_Util.c" \
"../Blink_Task.c" \
"../I2C_Task.c" \
"../MailBox_Util.c" \
"../StartBIOS.c" \
"../StartBIOS12.c" \
"../UART_Task.c" \
"../UART_Task_Miriam.c" 


