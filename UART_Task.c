/*
 *  ======== UART_Task.c ========
 *  Author: Michael Kramer / Matthias Wenzl
 */
#include <stdbool.h>
#include <stdint.h>
#include <inc/hw_memmap.h>
#include <string.h>

/* XDCtools Header files */
#include <xdc/std.h>
#include <xdc/cfg/global.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/Memory.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>

/* TI-RTOS Header files */
#include <driverlib/sysctl.h>
#include <ti/drivers/UART.h>

/* Driverlib headers */
#include <driverlib/gpio.h>
#include <driverlib/pin_map.h>

/*Board Header files */
#include <Board.h>
#include <EK_TM4C1294XL.h>

/* Application headers */
#include "UART_Task.h"
#include "MailBox_Util.h"

extern uint32_t factor;
extern uint16_t halfmode;
/*
 *  ======== UART  ========
 *  Echo Characters recieved and show reception on Port N Led 0
 */
void UARTFxn(UArg arg0, UArg arg1)
{
    UART_Handle uart;
    UART_Params uartParams;
    const char echoPrompt[] = "\fWelcome to 7Seg !\r\n ";

    /* Create a UART with data processing off. */
    UART_Params_init(&uartParams);
    uartParams.writeDataMode = UART_DATA_BINARY;
    uartParams.readDataMode = UART_DATA_BINARY;
    uartParams.readReturnMode = UART_RETURN_FULL;
    uartParams.readEcho = UART_ECHO_OFF;
    uartParams.baudRate = 9600;
    uart = UART_open(Board_UART0, &uartParams);


    if (uart == NULL) {
        System_abort("Error opening the UART");
    }

    UART_write(uart, echoPrompt, sizeof(echoPrompt));

    /* Loop forever echoing */
    /*while (1) {
        char input;
        UART_read(uart, &input, 1);
        GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_0, 1);
        if (input == 'h') {
            if (halfmode>2){
                halfmode=0;
            } else
                halfmode+=1;
        }
        UART_write(uart, &input, 1); // Remove this line to stop echoing!
        Task_sleep(5);
        GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_0, 0);
    }
    */

    //Salko 7Seg
    while (1) {
            char input[4];
            memset(input,0,sizeof(input));

            UART_read(uart, input, sizeof(input));
            if (input[0] == 'h') {
                        if (input[1] == 'l' && input[2] == 's'){
                            halfmode=1;
                        } else if(input [1] == 'r'  && input[2] == 's'){
                            halfmode=2;
                        } else if(input [1] == 'o'  && input[2] == 'f') {
                            halfmode = 0;
                        }
            }else if (input[0] == 'p'){
                MsgObj msg;
                msg.letter_1 = input[1];
                msg.letter_2 = input[2];
                System_printf("Sent %c and %c to 7Seg.\n",msg.letter_1, msg.letter_2);
                System_flush();
                writeToMailBox(&msg);
            }
            else if (input[0] == 'b' && input [1] == 'r'  && input[2] == 'i'){
                factor += 1;
                if(factor > 3) {
                    factor = 0;
                }
            }
            UART_write(uart, input, sizeof(input));
        }
}


/*
 *  Setup task function
 */
int setup_UART_Task(void)
{
    Task_Params taskUARTParams;
    Task_Handle taskUART;
    Error_Block eb;

    /* Enable and configure the peripherals used by the UART0 */
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
    GPIOPinConfigure(GPIO_PA0_U0RX);
    GPIOPinConfigure(GPIO_PA1_U0TX);
    GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);
    UART_init();

    /* Setup PortN LED1 activity signaling */
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPION);
    GPIOPinTypeGPIOOutput(GPIO_PORTN_BASE, GPIO_PIN_0);

    Error_init(&eb);
    Task_Params_init(&taskUARTParams);
    taskUARTParams.stackSize = 4096; /* stack in bytes */
    taskUARTParams.priority = 15; /* 0-15 (15 is highest priority on default -> see RTOS Task configuration) */
    taskUART = Task_create((Task_FuncPtr)UARTFxn, &taskUARTParams, &eb);
    if (taskUART == NULL) {
        System_abort("TaskUART create failed");
    }

    return (0);
}
