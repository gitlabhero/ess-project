/*
 * 7Seg_Task.c
 *
 *  Created on: 15.01.2018
 *      Author: salko.nuhanovic
 */

#include "7Seg_Task.h"

volatile uint32_t sysTickCounter = 0;
volatile uint32_t waitCounter = 0;
uint32_t pwmValues[4] = {5,15,25,55};
volatile uint32_t factor = 0;
volatile uint16_t halfmode = 0;
volatile uint16_t data;
volatile bool showWait = true;
volatile uint16_t index = 0;

int setup_7Seg_Task(int prio, xdc_String name){
    init();

    Task_Params taskLedParams;
    Task_Handle taskLed;
    Error_Block eb;

    /* Create blink task with priority 15*/
    Error_init(&eb);
    Task_Params_init(&taskLedParams);
    taskLedParams.instance->name = name;
    taskLedParams.stackSize = 1024; /* stack in bytes */
    taskLedParams.priority = prio; /* 0-15 (15 is highest priority on default -> see RTOS Task configuration) */
    taskLed = Task_create((Task_FuncPtr)SevenSegFxn, &taskLedParams, &eb);
    if (taskLed == NULL) {
        System_abort("TaskLed create failed");
    }

    Clock_Params clkParams;
    Clock_Params_init(&clkParams);
    clkParams.period = 1;
    clkParams.startFlag = TRUE;
    Clock_Handle hdl = Clock_create((Clock_FuncPtr) ClockFxn,10,&clkParams, &eb);
    if (Error_check(&eb)) {
        System_abort("Creating clock task failed");
    }

    return 0;
}

void ClockFxn(void) {
    uint16_t clearDisplay;

    if( (sysTickCounter % (100/pwmValues[factor])) == 0 ) {
        writeDataTo7Seg(data);
    }else if (halfmode == 1){
        clearDisplay = data & 65280;
        writeDataTo7Seg(clearDisplay);
    } else if (halfmode == 0){
        clearDisplay = 0;
        writeDataTo7Seg(clearDisplay);
    }else if (halfmode == 2){
        clearDisplay = data & 255;
        writeDataTo7Seg(clearDisplay);
    }

    sysTickCounter++;

    if (sysTickCounter >= 100) {
        sysTickCounter = 0;
    }

    waitCounter++;
    if (waitCounter > 500 && showWait == true) {
         waitCounter = 0;
         data = createWaitValue(index);
         index++;
     }

    if(index > 7){
        index = 0;
    }
}

void SevenSegFxn(){
    pendFromMailbox();
}

void onMessageReceived(MsgObj* msg){
    unsigned short digit_1 = 0;
    unsigned short digit_2 = 0;
    uint8_t help;
    char help1 = msg->letter_1;
    char help2 = msg->letter_2;

    if( (help1 >= '0') && (help1 <= '9') ){
        help = help1 - '0';
        digit_1 = GetDigit(help);
    }

    if ((help2 >= '0') && (help2 <= '9') ){
        help = help2 - '0';
        digit_2 = GetDigit(help);
    }

    if ((help1 >= 'A') && (help1 <= 'd')){
        help = GetChar(help1);
        digit_1 = help;
    }

    if ((help2 >= 'A') && (help2 <= 'd')){
        help = GetChar(help2);
        digit_2 = help;
    }

    data = prepareDatafor7Seg(digit_1,digit_2,false,true);
}
