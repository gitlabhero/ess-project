/*
 *  ======== StartBIOS.c ========
 */
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <inc/hw_memmap.h>

/* XDCtools Header files */
#include <xdc/std.h>
#include <xdc/cfg/global.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/Memory.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Event.h>

/* Currently unused RTOS headers that are needed
 * for advanced features like IPC. */
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/knl/Mailbox.h>
#include <ti/sysbios/knl/Event.h>
#include <ti/sysbios/hal/Timer.h>
#include <ti/sysbios/hal/Hwi.h>

/* Driverlib headers */
#include <driverlib/gpio.h>
#include <driverlib/sysctl.h>


/* Board Header files */
#include <Board.h>
#include <EK_TM4C1294XL.h>

/* Application headers */
#include "UART_Task.h"
#include "MailBox_Util.h"
#include "UART_Task.h"
#include "7Seg_Task.h"
#include "Blink_Task.h"
#include "I2C_Task.h"


uint32_t ui32SysClock;
int main(void) {
        ui32SysClock = Board_initGeneral(120 * 1000 * 1000);

        (void)initMailbox();

        setup_7Seg_Task(13, "7Seg_Task");
        System_printf("Start 7Seg Task\n");
        System_flush();


        (void)setup_Blink_Task();
        System_printf("Blink Task\n");
        System_flush();

        (void)setup_I2C_Task();
        System_printf("I2C Task\n");
        System_flush();

        (void)setup_UART_Task();
        System_printf("Created UART Task\n");
        System_flush();

        System_printf("Start BIOS\n");
        System_flush();
        BIOS_start();
}



