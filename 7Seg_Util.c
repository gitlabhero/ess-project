/*
 * 7Seg_Task.c
 *
 *  Created on: 15.01.2018
 *      Author: salko.nuhanovic
 */

#include "7Seg_Util.h"

extern uint32_t ui32SysClock;

const unsigned short CharTable[55]={
    0x80, // '-'
    0x01, // '.'   Sevensegment bit order
    0x00, // '/'   (g)(f)(e)(d)(c)(a)(b)(dp)
    0x7E, // '0'
    0x0A, // '1'    _a_
    0xB6, // '2'  f|   |b
    0x9E, // '3'   |_g_|
    0xCA, // '4'  e|   |c
    0xDC, // '5'   |_d_|.dp
    0xFC, // '6'
    0x0E, // '7'
    0xFE, // '8'
    0xDE, // '9'
    0x00, // ':'
    0x00, // ';'
    0x00, // '<'
    0x00, // '='
    0x00, // '>'
    0x00, // '?'
    0x00, // '@'
    0xEE, // 'A'
    0xF8, // 'B'
    0x74, // 'C'
    0xBA, // 'D'
    0xF4, // 'E'
    0xE4, // 'F'
    0x7C, // 'G'
    0xEA, // 'H'
    0x0A, // 'I'
    0x3A, // 'J'
    0x00, // 'K'
    0x70, // 'L'
    0x00, // 'M'
    0x6E, // 'N'
    0x00, // 'O'
    0xE6, // 'P'
    0xCE, // 'Q'
    0x64, // 'R'
    0xDC, // 'S'
    0xF0, // 'T'
    0x7A, // 'U'
    0x00, // 'V'
    0x00, // 'W'
    0x00, // 'X'
    0xDA, // 'Y'
    0x00, // 'Z'
    0x00, // '['
    0x00, // '/'
    0x00, // ']'
    0x00, // '^'
    0x10, // '_'
    0x00, // '''
    0xB8, // 'a'
    0xF8, // 'b'
    0xB0, // 'c'
    0xBA  // 'd'
};

const uint16_t waitIndicator[8]={
    0b0000010000000100,
    0b0000000001000100,
    0b0000000001100000,
    0b0000000000110000,
    0b0001000000010000,
    0b0001100000000000,
    0b0000101000000000,
    0b0000011000000000
};
void init() {
	Board_initSPI();
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOP);
	GPIOPinTypeGPIOOutput(GPIO_PORTP_BASE, GPIO_PIN_5);
	SSIClockSourceSet(SSI3_BASE, SSI_CLOCK_SYSTEM);
	SSIConfigSetExpClk(SSI3_BASE, ui32SysClock, SSI_FRF_MOTO_MODE_0,
	SSI_MODE_MASTER, 10000000, 16);
	SSIEnable(SSI3_BASE);
}

unsigned short GetChar(char Character){
  if ((Character >= '-') && (Character <= 'd'))
    return CharTable[Character-'-'];
  else
    return 0;
}

unsigned short GetDigit(unsigned short Digit){
  if ((Digit >= 0) && (Digit <= 9))
    return CharTable[Digit+3];
  else
    return 0;
}

void writeDataTo7Seg(uint16_t data) {
    GPIOPinWrite(GPIO_PORTP_BASE, GPIO_PIN_5, 0);
    SSIDataPut(SSI3_BASE, data);
    while(SSIBusy(SSI3_BASE));
    GPIOPinWrite(GPIO_PORTP_BASE, GPIO_PIN_5, 0xFF);
}

uint16_t prepareDatafor7Seg(uint8_t leftDisyplay,uint8_t rightDisplay, bool leftDecimal, bool rightDecimal){
    uint16_t ret;

    if(rightDecimal){
        rightDisplay |= 0x01;
    }
    ret = rightDisplay  << 8;

    if(leftDecimal){
        leftDisyplay |= 0x01;
    }
    ret |= leftDisyplay;

    return ret;
}

uint16_t createWaitValue(uint16_t index){
   return waitIndicator[index];
}
