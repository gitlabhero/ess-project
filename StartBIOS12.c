/*
 *  ======== StartBIOS.c ========
 */
#include <stdbool.h>
#include <stdint.h>
#include <inc/hw_memmap.h>

/* XDCtools Header files */
#include <xdc/std.h>
#include <xdc/cfg/global.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/Memory.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/drivers/UART.h>

/* Currently unused RTOS headers that are needed
 * for advanced features like IPC. */
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/knl/Mailbox.h>
#include <ti/sysbios/knl/Event.h>
#include <ti/sysbios/hal/Timer.h>

/* Driverlib headers */
#include <driverlib/gpio.h>
#include <driverlib/sysctl.h>
#include <driverlib/pwm.h>
#include <driverlib/systick.h>
#include "driverlib/pin_map.h"

/* Board Header files */
#include <Board.h>
#include <EK_TM4C1294XL.h>
#include <Mailbox_Util.h>

/* Application headers */
#include "Blink_Task.h"
#include "I2C_Task.h"
#include <UART_Task_Miriam.h>




int main_m(void)
{
		uint32_t ui32SysClock;
	    /* Call board init functions. */
	    ui32SysClock = Board_initGeneral(120*1000*1000);
	    (void)ui32SysClock; // We don't really need this (yet)

        /* Initialize+start Blink Task*/
        (void)setup_Blink_Task();
        /* System_printf() is VERY slow!*/
        System_printf("Blink Task\n");
        System_flush();

	    /*Initialize+start UART Task*/
	    (void)setup_UART_Task_Miriam();
	    System_printf("UART Task\n");

        /*Initialize+start I2C Task*/
        (void)setup_I2C_Task();
        System_printf("I2C Task\n");
        System_flush();

	    /* Initialize+start Mailbox Task */
	    (void)initMailbox();
	    System_printf("Mailbox Task\n");
	    System_flush();

	    /* SysMin will only print to the console upon calling flush or exit */
	    System_printf("Start BIOS\n");
	    System_flush();

	    /* Start BIOS */
	    BIOS_start();




}


