/*
 *  ======== I2C_Task.c ========
 *
 */
#include <stdbool.h>
#include <stdint.h>
#include <inc/hw_memmap.h>

/* XDCtools Header files */
#include <xdc/std.h>
#include <xdc/cfg/global.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/Memory.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Event.h>

/* Driverlib headers */
#include <driverlib/gpio.h>
#include <driverlib/pin_map.h>

/* Tiva C I2C driver */
#include <ti/drivers/I2C.h>
#include <ti/drivers/UART.h>

/* Board Header files */
#include <Board.h>
#include <EK_TM4C1294XL.h>
#include "I2C_Task.h"
#include <string.h>
#include <driverlib/sysctl.h>
#include <Mailbox_Util.h>
#include <UART_Task_Miriam.h>

// TCS3471 registers definition
#define SLAVE_ADDR      0x29// Slave Address for I2C
#define ENABLE_ADDR     0x80// Enable status and interrupts
#define _CONTROL        0x8F// Gain control register
#define _GAIN_x16       0x10	// Gain Resolution 16
#define _ATIME          0x81// RGBC ADC time
#define _CDATA          0x94	// Clear ADC low data register
#define _CDATAH         0x95	// Clear ADC high data register
#define _RDATA          0x96	// RED ADC low data register
#define _RDATAH         0x97	// RED ADC high data register
#define _GDATA          0x98	// GREEN ADC low data register
#define _GDATAH         0x99	// GREEN ADC high data register
#define _BDATA          0x9A	// BLUE ADC low data register
#define _BDATAH         0x9B	// BLUE ADC high data register

// Color flags
#define PURPLE_FLAG 1
#define BLUE_FLAG   2
#define CYAN_FLAG   3
#define GREEN_FLAG  4
#define PINK_FLAG   5
#define RED_FLAG    6
#define ORANGE_FLAG 7
#define YELLOW_FLAG 8


// Find maximal floating point value
#define MAX_FLOAT(a, b) (((a) > (b)) ? (a) : (b))

// Find minimal floating point value
#define MIN_FLOAT(a, b) (((a) < (b)) ? (a) : (b))


// Variable declaration
I2C_Handle      handle;
I2C_Params      i2cparams;
I2C_Transaction i2c;
char i, found_color, color_flag;
unsigned int Clear, Red, Green, Blue;
float hue, color_value, color_value_sum;
float Red_Ratio, Green_Ratio, Blue_Ratio;
unsigned int Red_Value, Green_Value, Blue_Value;
uint8_t writeBuffer[8];
uint8_t readBuffer[8];
MsgObj msg;


// Convert RGB values to HSL values
float RGB_To_HSL(float red, float green, float blue) {
    float volatile fmax, fmin, hue, saturation, luminance;

    fmax = MAX_FLOAT(MAX_FLOAT(red, green), blue);
    fmin = MIN_FLOAT(MIN_FLOAT(red, green), blue);

    luminance = fmax;
    if (fmax > 0)
        saturation = (fmax - fmin) / fmax;
    else
        saturation = 0;

    if (saturation == 0)
        hue = 0;
    else {
        if (fmax == red)
            hue = (green - blue) / (fmax - fmin);
        else if (fmax == green)
            hue = 2 + (blue - red) / (fmax - fmin);
        else
            hue = 4 + (red - green) / (fmax - fmin);
        hue = hue / 6;

        if (hue < 0)
            hue += 1;
    }
    return hue;
}

void configure_leds(){
    GPIOPinTypeGPIOOutput(GPIO_PORTE_BASE, GPIO_PIN_4);
    GPIOPinTypeGPIOOutput(GPIO_PORTH_BASE, GPIO_PIN_2);
    GPIOPinTypeGPIOOutput(GPIO_PORTM_BASE, GPIO_PIN_3);
}

void sendOveri2c(unsigned short address, unsigned short data){
    writeBuffer[0] = address;
    writeBuffer[1] = data;
    i2c.writeCount = 2;
    i2c.readCount = 0;
    if (!I2C_transfer(handle, &i2c)){
        System_abort("Error\n");
    }
    System_flush();
}

//geklaut aus der demo - vielen dank für die blumen
void I2CFxn(void){

    I2C_Params_init(&i2cparams);
    i2cparams.bitRate = I2C_100kHz;
    i2cparams.transferMode = I2C_MODE_BLOCKING;


    // I2C7 für boosterpack eins
    handle = I2C_open(EK_TM4C1294XL_I2C7 , &i2cparams);
    if (handle == NULL) {
        System_abort("I2C was not opened");
    }else{
        System_printf("I2C Initialized!\n");
    }

    i2c.slaveAddress = SLAVE_ADDR;
    i2c.readCount = 0;
    memset(readBuffer,0,4);
    i2c.readBuf = (uint8_t*)&readBuffer[0];
    i2c.writeBuf = writeBuffer;
    /*  Register to write -> to enable */
    sendOveri2c(ENABLE_ADDR, 0x1B);
    sendOveri2c(_CONTROL, _GAIN_x16);
    sendOveri2c(_ATIME, 0x00);

    configure_leds();

    while (1) {
        unsigned short low_byte_c, low_byte_r, low_byte_g, low_byte_b;

        // Reset Color value sum variable
        color_value_sum = 0;

        // Get the average color value of 16 measurements
        for (i = 0; i < 16; i++) {

            // Read Clear, Red, Green and Blue channel register values
            i2c.writeCount = 8;
            i2c.readCount = 8;

            writeBuffer[0] = _CDATA;
            writeBuffer[1] = _CDATAH;
            writeBuffer[2] = _RDATA;
            writeBuffer[3] = _RDATAH;
            writeBuffer[4] = _GDATA;
            writeBuffer[5] = _GDATAH;
            writeBuffer[6] = _BDATA;
            writeBuffer[7] = _BDATAH;
            if (!I2C_transfer(handle, &i2c))
                System_abort("Bad I2C transfer!");

            //read low bytes
            low_byte_c = readBuffer[0];
            low_byte_r = readBuffer[2];
            low_byte_g = readBuffer[4];
            low_byte_b = readBuffer[6];

            //read values
            Clear = readBuffer[1];
            Red = readBuffer[3];
            Green = readBuffer[5];
            Blue = readBuffer[7];

            Clear = (Clear << 8);
            Clear = (Clear | low_byte_c);
            Red = (Red << 8);
            Red = (Red | low_byte_r);
            Green = (Green << 8);
            Green = (Green | low_byte_g);
            Blue = (Blue << 8);
            Blue = (Blue | low_byte_b);

            // Divide Red, Green and Blue values with Clear value
            Red_Ratio = ((float) Red / (float) Clear);
            Green_Ratio = ((float) Green / (float) Clear);
            Blue_Ratio = ((float) Blue / (float) Clear);

            // Convert RGB values to HSL values
            color_value = RGB_To_HSL(Red_Ratio, Green_Ratio, Blue_Ratio);

            // Sum the color values
            color_value_sum = color_value_sum + color_value;
        }

        // Get the average color value of 16 measurements
        color_value = color_value_sum / 16.0;

        //find the color given by the led

        if ((color_value >= 0.992) && (color_value <= 0.999)) {
            found_color = 1;
            if (color_flag != ORANGE_FLAG){
                color_flag = ORANGE_FLAG;
                msg.letter_1 ='0';
                msg.letter_2 ='R';
                writeToMailBox(&msg);
                //setUART(msg);
                System_printf("Orange\n");
            }
        }

        else if ((color_value >= 0.9750) && (color_value <= 0.9919)) {
            found_color = 1;
            if (color_flag != RED_FLAG){
                color_flag = RED_FLAG;
                msg.letter_1 ='R';
                msg.letter_2 ='E';
                writeToMailBox(&msg);
                //setUART(msg);
                System_printf("Red\n");
            }
        }

        else if ((color_value >= 0.920) && (color_value <= 0.9749)) {
            found_color = 1;
            if (color_flag != PINK_FLAG){
                color_flag = PINK_FLAG;
                msg.letter_1 ='P';
                msg.letter_2 ='I';
                writeToMailBox(&msg);
                //setUART(msg);
                System_printf("Pink\n");
            }
        }

        else if ((color_value >= 0.6201) && (color_value <= 0.919)) {
            found_color = 1;
            if (color_flag != PURPLE_FLAG){
                color_flag = PURPLE_FLAG;
                //color_flag = 0, verhindert dass nur einmal eingelesen wird
                msg.letter_1 ='P';
                msg.letter_2 ='U';
                writeToMailBox(&msg);
                //setUART(msg);
                System_printf("Purple\n");
            }
        }

        else if ((color_value >= 0.521) && (color_value <= 0.6200)) {
            found_color = 1;
            if (color_flag != BLUE_FLAG){
                color_flag = BLUE_FLAG;
                msg.letter_1 ='B';
                msg.letter_2 ='U';
                writeToMailBox(&msg);
                //setUART(msg);
                System_printf("Blue\n");
            }
        }

        else if ((color_value >= 0.470) && (color_value < 0.520)) {
            found_color = 1;
            if (color_flag != CYAN_FLAG){
                color_flag = CYAN_FLAG;
                msg.letter_1 ='C';
                msg.letter_2 ='Y';
                writeToMailBox(&msg);
                //setUART(msg);
                System_printf("Cyan\n");
            }
        }

        else if ((color_value >= 0.210) && (color_value <= 0.469)) {
            found_color = 1;
            if (color_flag != GREEN_FLAG){
                color_flag = GREEN_FLAG;
                msg.letter_1 ='G';
                msg.letter_2 ='R';
                writeToMailBox(&msg);
                //setUART(msg);
                System_printf("Green\n");
            }
        }

        else if ((color_value >= 0.0650) && (color_value <= 0.1800)) {
            found_color = 1;
            if (color_flag != YELLOW_FLAG){
                color_flag = YELLOW_FLAG;
                msg.letter_1 ='Y';
                msg.letter_2 ='E';
                writeToMailBox(&msg);
                //setUART(msg);
                System_printf("Yellow\n");
            }
        }

        // Color not in range
        else {
            if (found_color == 0){
                color_flag = 0;
                //msg.letter_1 ='E';
                //msg.letter_1 ='E';
                //writeToMailBox(&msg);
                //setUART(msg);
                System_printf("Color not in range.\n");
            }
        }

        System_flush();
        Task_sleep(1000);
    }


}

int setup_I2C_Task(void)
{
    Task_Params I2C_Params;
    Task_Handle I2C_Handle;
    Error_Block eb;

    Board_initI2C();

    /* Create i2c task with priority 15*/
    Error_init(&eb);
    Task_Params_init(&I2C_Params);
    I2C_Params.stackSize = 1024; /* stack in bytes */
    I2C_Params.priority = 15; /* 0-15 (15 is highest priority on default -> see RTOS Task configuration) */
    I2C_Handle = Task_create((Task_FuncPtr)I2CFxn, &I2C_Params, &eb);
    if (I2C_Handle == NULL) {
        System_abort("I2C Task create failed");
    }


    return (0);
}
